//
// Created by Sasha on 25.10.2023.
//
#include "../include/bmp.h"
#include <stdio.h>

#define PIXEL_SIZE sizeof(struct pixel)
#define BYTE_ALIGNMENT 4
#define BMP_TYPE 0x4D42
#define ZERO 0
#define BIT_COUNT 24
#define BI_SIZE 40
#define BI_PLANES 1

size_t mb_add_empty_bytes(const struct image* image){
    return BYTE_ALIGNMENT - (image->width * PIXEL_SIZE) % BYTE_ALIGNMENT;
}
enum read_status check(size_t result, struct bmp_header header) {
    if (result == 1){
        return READ_SUCCESS;
    }else{
        if (header.bfType != BMP_TYPE){
            return READ_INVALID_HEADER;
        }
        if (header.biHeight < 1 || header.biWidth <  1){
            return READ_INVALID_SIGNATURE;
        }
        return READ_INVALID_BITS;
    }
}
struct bmp_header set_bmp_header(struct image* image){
    size_t size_bmp_header = sizeof (struct bmp_header);

    uint64_t pixels = image->height * image->width * sizeof (struct pixel);

    size_t empty = mb_add_empty_bytes(image);

    uint64_t size_of_empty = sizeof (char) * image->height * empty;

    struct bmp_header return_bmp_header = {0};

    uint64_t pixelSize = pixels + size_of_empty;

    return_bmp_header.bfType = BMP_TYPE;
    return_bmp_header.bfileSize = pixelSize + size_bmp_header;
    return_bmp_header.bfReserved = ZERO;
    return_bmp_header.bOffBits = size_bmp_header;
    return_bmp_header.biSize = BI_SIZE;
    return_bmp_header.biWidth = image->width;
    return_bmp_header.biHeight = image->height;
    return_bmp_header.biPlanes = BI_PLANES;
    return_bmp_header.biBitCount = BIT_COUNT;
    return_bmp_header.biCompression = ZERO;
    return_bmp_header.biSizeImage = pixelSize;
    return_bmp_header.biXPelsPerMeter = ZERO;
    return_bmp_header.biYPelsPerMeter = ZERO;
    return_bmp_header.biClrUsed = ZERO;
    return_bmp_header.biClrImportant = ZERO;


    return return_bmp_header;
}

struct image set_pixels_to_image(FILE* input, struct image* image, size_t empty){
    const uint64_t height = image->height;
    const uint64_t width = image->width;

    for (uint32_t i = 0; i < height; i++) {
        const uint64_t index = width * i;
        const size_t size = PIXEL_SIZE;


        fread(&(image)->data[index], size, width, input);
        if (feof(input) || ferror(input)){
            return (struct image) {0};
        }
        /* Skipping trash bytes */
        fseek(input, (long)empty, 1);
    }
    return *image;
}
enum read_status from_bmp(FILE* input, struct image* image ) {
    struct bmp_header header;
    size_t read_result = fread(&header, sizeof(struct bmp_header), 1, input);
    //Checking header for validity
    enum read_status result = check(read_result, header);

    if (result != READ_SUCCESS){
        return result;
    }else{


        struct maybe_image mb_image = create_image(header.biWidth, header.biHeight);
        if (mb_image.valid){
            *image = mb_image.image;
        }else{
            return READ_NO_MEMORY;
        }
        if (image == NULL){
            return READ_NO_MEMORY;
        }
        /* Calculation of trash bytes */
        size_t empty = mb_add_empty_bytes(image);

        /* Transferring pixels from an input file to a struct image, taking into account trash bytes. */
        set_pixels_to_image(input, image, empty);

        return result;
    }
}

enum write_status to_bmp(FILE* output, struct image* image){
    uint64_t width = image->width;
    size_t empty = mb_add_empty_bytes(image);
    struct bmp_header header = set_bmp_header(image);
    fwrite(&header, sizeof(struct bmp_header), 1, output);
    for (uint32_t i = 0; i < image->height; i++) {
        size_t index = i * width;
        size_t write = fwrite(&image->data[index], PIXEL_SIZE, width, output);
        if (write != image->width){
            return WRITE_ERROR;
        }
        for (uint32_t j = 0; j < empty; j++) {
           fputc(0, output);
        }
    }
    return WRITE_OK;
}
