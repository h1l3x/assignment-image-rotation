﻿//
// Created by Sasha on 25.10.2023.
//
#include "../include/file_manager.h"

struct file open_file(char* filename, const char* mode) {
    FILE* file = fopen(filename, mode);
    if (!file){
        return (struct file) {0, .status = ERROR_ON_READ};
    }

    return (struct file) { .file=file, .status=SUCCESS };
}
