//
// Created by Sasha on 26.12.2023.
//
#include <stdio.h>

void show_err(const char *description){
    fprintf(stderr, "ERROR: %s.",
            description);
}
