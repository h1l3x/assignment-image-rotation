//
// Created by Sasha on 28.10.2023.
//
#include "../include/image.h"

void trans_image(struct image* image, struct maybe_image mb_trans_image){
    for (int row = 0; row < image->height; row++){
        for (int col = 0; col < image->width; col++){
            uint64_t index = get_index_by_coordinate(image, col, row);
            struct maybe_pixel mb_pixel = get_pixel_from_image(image, index);
            uint64_t trans_index = get_index_by_coordinate(&mb_trans_image.image, row, col);
            if (mb_pixel.valid){
                set_pixel_in_image(&mb_trans_image.image, mb_pixel.pixel, trans_index);
            }
        }
    }
}

void return_image(struct maybe_image mb_return_img, struct maybe_image mb_trans_image){
    for (int x = 0; x < mb_trans_image.image.width; x++){
        for (int y = 0; y < mb_trans_image.image.height; y++){
            uint64_t index = get_index_by_coordinate(&mb_trans_image.image, x, y);
            struct maybe_pixel mb_pixel = get_pixel_from_image(&mb_trans_image.image, index);
            if (mb_pixel.valid) {
                /*We start the search from the end.*/
                uint64_t new_index = get_index_by_coordinate(&mb_return_img.image, mb_trans_image.image.width - x - 1, y);
                set_pixel_in_image(&mb_return_img.image, mb_pixel.pixel, new_index);
            }
        }
    }
}

struct image rotate_image(struct image* image){
    /* We set the image size to the opposite of the original one.*/
    /* (the width became the height, and the height became the width).*/
    struct maybe_image mb_trans_image = create_image(image->height, image->width);
    if (mb_trans_image.valid){
        /*Transpose the "matrix" of pixels. */
        trans_image(image, mb_trans_image);
        /*The image that is in trans is the original image rotated 90 degrees to the right.*/
        /* (Consequence of transposition)*/
        /*Next, we need to change the "columns" of pixels between each other so that the final image takes the correct form.*/
        struct maybe_image mb_return_img = create_image(image->height, image->width);
        if (mb_return_img.valid){
            return_image(mb_return_img, mb_trans_image);
        }
        delete_image(&mb_trans_image.image);
        return mb_return_img.image;
    }
    return *image;
}
