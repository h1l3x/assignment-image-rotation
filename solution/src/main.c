#include "../include/bmp.h"
#include "../include/file_manager.h"
#include "../include/rotate.h"
#include "../include/util.h"

#include <stdio.h>

int main( int argc, char** argv ) {
    (void) argc; (void) argv; // supress 'unused parameters' warning
    if (argc != 3){
        show_err("Incorrect number of arguments passed");
        return 1;
    }else{
        struct file input = open_file(argv[1], "rb");
        if (input.status == ERROR_ON_READ){
            show_err("Input File. Error when opening the source image");
            fclose(input.file);
            return 2;
        }

        struct image image = {0};
        /* Reading an image from a file into a struct image */
        enum read_status read_result = from_bmp(input.file, &image);

        fclose(input.file);
        if (read_result == READ_SUCCESS){
            struct image rotate = rotate_image(&image);
            if (image.data == rotate.data){
                delete_image(&image);
                delete_image(&rotate);
                show_err("Error when rotating the image");
                return 3;
            }else{
                delete_image(&image);
                struct file output = open_file(argv[2], "wb");
                if (input.status == ERROR_ON_READ){
                    show_err("Output File. Error when opening the source image");
                    fclose(output.file);
                    delete_image(&rotate);
                    return 4;
                }else{
                    if (to_bmp(output.file, &rotate) == WRITE_OK){
                        delete_image(&rotate);
                        return 0;
                    }else{
                        delete_image(&rotate);
                        return 5;
                    }
                }
            }
        }else{
            delete_image(&image);
            return read_result;
        }
    }
}
