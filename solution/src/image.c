//
// Created by Sasha on 25.10.2023.
//
#include "../include/image.h"
#include "stdbool.h"
#include <malloc.h>


uint64_t get_index_by_coordinate(struct image* image, uint64_t x, uint64_t y){
    return x + y * image->width;
}

struct maybe_pixel get_pixel_from_image(struct image* image, uint64_t index){
    //Checking that values that are exactly outside the image size have not been passed.
    if (index < image->width * image->height){

        return (struct maybe_pixel) {
            &image->data[index],
            .valid = true
        };
    }else{
        return (struct maybe_pixel) {0};
    }
}

bool set_pixel_in_image(struct image* image, struct pixel* pixel, uint64_t index){
    //Checking that values that are exactly outside the image size have not been passed.
    if (index < image->width * image->height) {
        image->data[index] = *pixel;
        return true;
    } else {
        return false;
    }
}

struct maybe_image create_image(const uint64_t width, const uint64_t height){
    struct image image = {0};
    image.width = width;
    image.height = height;
    image.data = malloc(width * height * sizeof(struct pixel));
    if (image.data != NULL){
        return (struct maybe_image){
                .image = image,
                .valid = true
        };
    }
    else{
        free(image.data);
        return (struct maybe_image) {0};
    }
}

void delete_image(struct image* image){
    free(image->data);
    image->width = 0;
    image->height = 0;
}


