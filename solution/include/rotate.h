//
// Created by Sasha on 28.10.2023.
//

#ifndef IMAGE_TRANSFORMER_ROTATE_H
#define IMAGE_TRANSFORMER_ROTATE_H
#include "image.h"

struct image rotate_image(struct image* image);

#endif //IMAGE_TRANSFORMER_ROTATE_H
