//
// Created by Sasha on 25.10.2023.
//

#ifndef LAB1_IMAGE_H
#define LAB1_IMAGE_H

#include "stdint.h"

#include <stdbool.h>



struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct pixel {
    uint8_t r, g, b;
};
struct maybe_image{
    struct image image;
    bool valid;
};
struct maybe_pixel{
    struct pixel* pixel;
    bool valid;
};
struct maybe_image create_image(uint64_t width, uint64_t height);

struct maybe_pixel get_pixel_from_image(struct image* image, uint64_t index);

bool set_pixel_in_image(struct image* image, struct pixel* pixel, uint64_t index);

uint64_t get_index_by_coordinate(struct image* image, uint64_t x, uint64_t y);

void delete_image(struct image* image);
#endif //LAB1_IMAGE_H
