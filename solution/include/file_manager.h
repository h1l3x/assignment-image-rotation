//
// Created by Sasha on 25.10.2023.
//

#ifndef LAB1_FILE_MANAGER_H
#define LAB1_FILE_MANAGER_H


#include "bmp.h"
#include "image.h"
#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>

enum read_file_status {
    SUCCESS = 0,
    ERROR_ON_READ
};

struct file {
    FILE* file;
    enum read_file_status status;
};

struct file open_file(char* filename, const char* mode);


#endif //LAB1_FILE_MANAGER_H
